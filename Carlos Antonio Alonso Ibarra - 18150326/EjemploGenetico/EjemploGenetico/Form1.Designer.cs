﻿
namespace EjemploGenetico
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.NUDPob = new System.Windows.Forms.NumericUpDown();
            this.BTNInic = new System.Windows.Forms.Button();
            this.RTBPoblacion = new System.Windows.Forms.RichTextBox();
            this.BTNEval = new System.Windows.Forms.Button();
            this.BTNSelec = new System.Windows.Forms.Button();
            this.RTBEval = new System.Windows.Forms.RichTextBox();
            this.RTBSele = new System.Windows.Forms.RichTextBox();
            this.NUDPres = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.RTBCruzar = new System.Windows.Forms.RichTextBox();
            this.BTNCruzar = new System.Windows.Forms.Button();
            this.RTBMutar = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.NUDMutacion = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDMutacion)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tamaño de la población";
            // 
            // NUDPob
            // 
            this.NUDPob.Location = new System.Drawing.Point(139, 7);
            this.NUDPob.Name = "NUDPob";
            this.NUDPob.Size = new System.Drawing.Size(88, 20);
            this.NUDPob.TabIndex = 1;
            this.NUDPob.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // BTNInic
            // 
            this.BTNInic.Location = new System.Drawing.Point(83, 42);
            this.BTNInic.Name = "BTNInic";
            this.BTNInic.Size = new System.Drawing.Size(75, 23);
            this.BTNInic.TabIndex = 2;
            this.BTNInic.Text = "Inicializar";
            this.BTNInic.UseVisualStyleBackColor = true;
            this.BTNInic.Click += new System.EventHandler(this.BTNInic_Click);
            // 
            // RTBPoblacion
            // 
            this.RTBPoblacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RTBPoblacion.Location = new System.Drawing.Point(12, 71);
            this.RTBPoblacion.Name = "RTBPoblacion";
            this.RTBPoblacion.ReadOnly = true;
            this.RTBPoblacion.Size = new System.Drawing.Size(212, 346);
            this.RTBPoblacion.TabIndex = 3;
            this.RTBPoblacion.Text = "";
            // 
            // BTNEval
            // 
            this.BTNEval.Location = new System.Drawing.Point(313, 42);
            this.BTNEval.Name = "BTNEval";
            this.BTNEval.Size = new System.Drawing.Size(75, 23);
            this.BTNEval.TabIndex = 4;
            this.BTNEval.Text = "Evaluar";
            this.BTNEval.UseVisualStyleBackColor = true;
            this.BTNEval.Click += new System.EventHandler(this.BTNEval_Click);
            // 
            // BTNSelec
            // 
            this.BTNSelec.Location = new System.Drawing.Point(539, 42);
            this.BTNSelec.Name = "BTNSelec";
            this.BTNSelec.Size = new System.Drawing.Size(75, 23);
            this.BTNSelec.TabIndex = 5;
            this.BTNSelec.Text = "Seleccionar";
            this.BTNSelec.UseVisualStyleBackColor = true;
            this.BTNSelec.Click += new System.EventHandler(this.BTNSelec_Click);
            // 
            // RTBEval
            // 
            this.RTBEval.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RTBEval.Location = new System.Drawing.Point(240, 71);
            this.RTBEval.Name = "RTBEval";
            this.RTBEval.ReadOnly = true;
            this.RTBEval.Size = new System.Drawing.Size(212, 346);
            this.RTBEval.TabIndex = 6;
            this.RTBEval.Text = "";
            // 
            // RTBSele
            // 
            this.RTBSele.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RTBSele.Location = new System.Drawing.Point(470, 71);
            this.RTBSele.Name = "RTBSele";
            this.RTBSele.ReadOnly = true;
            this.RTBSele.Size = new System.Drawing.Size(212, 346);
            this.RTBSele.TabIndex = 7;
            this.RTBSele.Text = "";
            // 
            // NUDPres
            // 
            this.NUDPres.Location = new System.Drawing.Point(437, 5);
            this.NUDPres.Name = "NUDPres";
            this.NUDPres.Size = new System.Drawing.Size(88, 20);
            this.NUDPres.TabIndex = 9;
            this.NUDPres.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(389, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Presión";
            // 
            // RTBCruzar
            // 
            this.RTBCruzar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RTBCruzar.Location = new System.Drawing.Point(701, 71);
            this.RTBCruzar.Name = "RTBCruzar";
            this.RTBCruzar.ReadOnly = true;
            this.RTBCruzar.Size = new System.Drawing.Size(212, 346);
            this.RTBCruzar.TabIndex = 11;
            this.RTBCruzar.Text = "";
            // 
            // BTNCruzar
            // 
            this.BTNCruzar.Location = new System.Drawing.Point(770, 42);
            this.BTNCruzar.Name = "BTNCruzar";
            this.BTNCruzar.Size = new System.Drawing.Size(75, 23);
            this.BTNCruzar.TabIndex = 10;
            this.BTNCruzar.Text = "Cruzar";
            this.BTNCruzar.UseVisualStyleBackColor = true;
            this.BTNCruzar.Click += new System.EventHandler(this.BTNCruzar_Click);
            // 
            // RTBMutar
            // 
            this.RTBMutar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RTBMutar.Location = new System.Drawing.Point(931, 71);
            this.RTBMutar.Name = "RTBMutar";
            this.RTBMutar.ReadOnly = true;
            this.RTBMutar.Size = new System.Drawing.Size(212, 346);
            this.RTBMutar.TabIndex = 13;
            this.RTBMutar.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1000, 42);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Mutar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // NUDMutacion
            // 
            this.NUDMutacion.DecimalPlaces = 1;
            this.NUDMutacion.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.NUDMutacion.Location = new System.Drawing.Point(1045, 10);
            this.NUDMutacion.Name = "NUDMutacion";
            this.NUDMutacion.Size = new System.Drawing.Size(88, 20);
            this.NUDMutacion.TabIndex = 15;
            this.NUDMutacion.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(912, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Probabilidad de Mutación";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1153, 435);
            this.Controls.Add(this.NUDMutacion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RTBMutar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.RTBCruzar);
            this.Controls.Add(this.BTNCruzar);
            this.Controls.Add(this.NUDPres);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RTBSele);
            this.Controls.Add(this.RTBEval);
            this.Controls.Add(this.BTNSelec);
            this.Controls.Add(this.BTNEval);
            this.Controls.Add(this.RTBPoblacion);
            this.Controls.Add(this.BTNInic);
            this.Controls.Add(this.NUDPob);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.NUDPob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDMutacion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NUDPob;
        private System.Windows.Forms.Button BTNInic;
        private System.Windows.Forms.RichTextBox RTBPoblacion;
        private System.Windows.Forms.Button BTNEval;
        private System.Windows.Forms.Button BTNSelec;
        private System.Windows.Forms.RichTextBox RTBEval;
        private System.Windows.Forms.RichTextBox RTBSele;
        private System.Windows.Forms.NumericUpDown NUDPres;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox RTBCruzar;
        private System.Windows.Forms.Button BTNCruzar;
        private System.Windows.Forms.RichTextBox RTBMutar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown NUDMutacion;
        private System.Windows.Forms.Label label3;
    }
}

