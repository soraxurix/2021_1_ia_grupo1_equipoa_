﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjemploGenetico
{
    public partial class Form1 : Form
    {
        Poblacion LaPoblacion;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void BTNInic_Click(object sender, EventArgs e)
        {
            LaPoblacion = new Poblacion((int)NUDPob.Value);
            RTBPoblacion.Text = LaPoblacion.GetDatosPoblacion();
        }

        private void BTNEval_Click(object sender, EventArgs e)
        {
            LaPoblacion.Evaluacion();
            RTBEval.Text = LaPoblacion.GetDatosPoblacion();
        }

        private void BTNSelec_Click(object sender, EventArgs e)
        {
            LaPoblacion.Seleccion((double)NUDPres.Value);
            RTBSele.Text = LaPoblacion.GetDatosPoblacion();
        }

        private void BTNCruzar_Click(object sender, EventArgs e)
        {
            LaPoblacion.Cruzamiento();
            RTBCruzar.Text = LaPoblacion.GetDatosPoblacion();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LaPoblacion.Mutacion((double)NUDMutacion.Value);
            RTBMutar.Text = LaPoblacion.GetDatosPoblacion();
        }

    }
}
