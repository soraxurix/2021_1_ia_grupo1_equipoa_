﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjemploGenetico
{
    public partial class Form1 : Form
    {
        private matrices matrices;
        Poblacion LaPoblacion;
        public Form1()
        {
            InitializeComponent();
            matrices = new matrices();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void BTNInic_Click(object sender, EventArgs e)
        {
            
            LaPoblacion = new Poblacion((int)NUDPob.Value);
            RTBPoblacion.Text = LaPoblacion.GetDatosPoblacion();


        }

        private void BTNEval_Click(object sender, EventArgs e)
        {
            LaPoblacion.Evaluacion();
            RTBEval.Text = LaPoblacion.GetDatosPoblacion();
        }

        private void BTNSelec_Click(object sender, EventArgs e)
        {
            LaPoblacion.Seleccion((double)NUDPres.Value);
            RTBSele.Text = LaPoblacion.GetDatosPoblacion();
        }

        private void BTNCruzar_Click(object sender, EventArgs e)
        {
            LaPoblacion.Cruzamiento();
            RTBCruzar.Text = LaPoblacion.GetDatosPoblacion();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LaPoblacion.Mutacion((double)NUDMutacion.Value);
            RTBMutar.Text = LaPoblacion.GetDatosPoblacion();
        }

        private void buttonCargarCSV_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //Se lee el archivo CSV y analiza para la creación de la matriz.
                string[] lineas = File.ReadAllLines(openFileDialog1.FileName);
                int contadorFilas = 0;
                int contadorColumnas = 0;
                foreach (var linea in lineas)//Se definen las dimensiones de la matriz.
                {
                    contadorFilas++;
                    var valores = linea.Split(',');
                    contadorColumnas = valores.Length;
                }

                //MessageBox.Show(contadorFilas.ToString()+" - "+ contadorColumnas);

                string[,] MatrizPrincipal = new string[contadorFilas, contadorColumnas];

                for (int i = 0; i < lineas.Length; i++)//Se llena la matriz
                {
                    var linea = lineas[i];
                    var valores = linea.Split(',');
                    for (int j = 0; j < valores.Length; j++)
                    {
                        MatrizPrincipal[i, j] = valores[j];
                    }
                }
                matrices.setmatricesEstados(MatrizPrincipal);
                DisplayData(dataMatrizMostrar, MatrizPrincipal);
            }
        }

        public void DisplayData(DataGridView tabla, string[,] matriz)//Toma la información de las matrices y las muestra en el datagrid
        {
            tabla.ColumnCount = matriz.GetLength(1);
            tabla.RowCount = matriz.GetLength(0);

            for (int i = 0; i < matriz.GetLength(0); i++)//Se llena el datagrid
            {
                for (int j = 0; j < matriz.GetLength(1); j++)
                {
                    tabla.Rows[i].Cells[j].Value = matriz[i, j];
                }
            }

            tabla.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            tabla.ColumnHeadersVisible = false;
            tabla.RowHeadersVisible = false;
        }
    }
}
