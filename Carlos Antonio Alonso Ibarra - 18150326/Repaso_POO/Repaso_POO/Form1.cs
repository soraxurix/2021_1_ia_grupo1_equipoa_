﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Repaso_POO
{
    public partial class Form1 : Form
    {
        CuentaBancaria Cuenta;
        double Monto;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        //Funciones del boton Aceptar
        private void BTAceptar_Click(object sender, EventArgs e)
        {
            Monto = Convert.ToDouble(NUDCantidad.Value);
            double saldodespues=0;
            if(CBDeposito.Checked)
            {
                var result = MessageBox.Show("¿Seguro que desea realizar está operación?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    //Cuenta.Depositar(Monto);
                    saldodespues = Convert.ToDouble(NUDSaldo.Value) + Monto;
                }
                MessageBox.Show("Se han agregado: " + Monto + " a su cuenta, su saldo actual es: "+ saldodespues);
                LimpiaryHabilitar();
            }
            else 
            {
                var result = MessageBox.Show("¿Seguro que desea realizar está operación?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                   //Cuenta.Retirar(Monto);
                   saldodespues = Convert.ToDouble(NUDSaldo.Value) - Monto;
                }
                MessageBox.Show("Se han retirado: " + Monto + " a su cuenta. su saldo actual es: "+ saldodespues);
                LimpiaryHabilitar();
            }
            
            
        }
        //Funciones del boton Cancelar
        private void BTCancelar_Click(object sender, EventArgs e)
        {
            
            var result = MessageBox.Show("¿Seguro que desea cancelar está operación?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            
        }

        //Función de habilitado al seleccionar un checkbox
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CBRetiro.Enabled = false;
            MessageBox.Show("Ha seleccionado depositar");
        }

        private void CBRetiro_CheckedChanged(object sender, EventArgs e)
        {
            CBDeposito.Enabled = false;
            MessageBox.Show("Ha seleccionado retirar");
        }

        public void LimpiaryHabilitar()
        {
            CBRetiro.Enabled = true;
            CBRetiro.CheckState = CheckState.Unchecked;
            CBDeposito.Enabled = true;
            CBDeposito.CheckState = CheckState.Unchecked;
            NUDCantidad.Value = 0;
        }



        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
