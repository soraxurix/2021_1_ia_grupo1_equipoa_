﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repaso_POO
{
    class CuentaBancaria
    {
        string NombreCliente, NumeroCuenta;
        double Saldo, Cantidad;

        public CuentaBancaria(string NombreCliente, string NumeroCuenta, double Saldo, double Cantidad)
        {
            this.NombreCliente = NombreCliente;
            this.NumeroCuenta = NumeroCuenta;
            this.Saldo = Saldo;
            this.Cantidad = Cantidad;
        }

        public void Depositar(double Monto)
        {
            Saldo += Monto;
        }

        public void Retirar(double Monto)
        {
            Saldo -= Monto;
        }
    }
}
