﻿
namespace Repaso_POO
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TBNombre = new System.Windows.Forms.TextBox();
            this.TBNumDeCuenta = new System.Windows.Forms.TextBox();
            this.NUDSaldo = new System.Windows.Forms.NumericUpDown();
            this.BTAceptar = new System.Windows.Forms.Button();
            this.BTCancelar = new System.Windows.Forms.Button();
            this.CBDeposito = new System.Windows.Forms.CheckBox();
            this.CBRetiro = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.NUDCantidad = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.NUDSaldo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCantidad)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Numero de cuenta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Saldo";
            // 
            // TBNombre
            // 
            this.TBNombre.Location = new System.Drawing.Point(129, 18);
            this.TBNombre.Name = "TBNombre";
            this.TBNombre.Size = new System.Drawing.Size(215, 20);
            this.TBNombre.TabIndex = 3;
            // 
            // TBNumDeCuenta
            // 
            this.TBNumDeCuenta.Location = new System.Drawing.Point(129, 53);
            this.TBNumDeCuenta.Name = "TBNumDeCuenta";
            this.TBNumDeCuenta.Size = new System.Drawing.Size(215, 20);
            this.TBNumDeCuenta.TabIndex = 4;
            // 
            // NUDSaldo
            // 
            this.NUDSaldo.Enabled = false;
            this.NUDSaldo.Location = new System.Drawing.Point(129, 94);
            this.NUDSaldo.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.NUDSaldo.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.NUDSaldo.Name = "NUDSaldo";
            this.NUDSaldo.Size = new System.Drawing.Size(215, 20);
            this.NUDSaldo.TabIndex = 5;
            this.NUDSaldo.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // BTAceptar
            // 
            this.BTAceptar.Location = new System.Drawing.Point(237, 211);
            this.BTAceptar.Name = "BTAceptar";
            this.BTAceptar.Size = new System.Drawing.Size(75, 23);
            this.BTAceptar.TabIndex = 6;
            this.BTAceptar.Text = "Aceptar";
            this.BTAceptar.UseVisualStyleBackColor = true;
            this.BTAceptar.Click += new System.EventHandler(this.BTAceptar_Click);
            // 
            // BTCancelar
            // 
            this.BTCancelar.Location = new System.Drawing.Point(384, 211);
            this.BTCancelar.Name = "BTCancelar";
            this.BTCancelar.Size = new System.Drawing.Size(75, 23);
            this.BTCancelar.TabIndex = 7;
            this.BTCancelar.Text = "Cancelar";
            this.BTCancelar.UseVisualStyleBackColor = true;
            this.BTCancelar.Click += new System.EventHandler(this.BTCancelar_Click);
            // 
            // CBDeposito
            // 
            this.CBDeposito.AutoSize = true;
            this.CBDeposito.Location = new System.Drawing.Point(237, 169);
            this.CBDeposito.Name = "CBDeposito";
            this.CBDeposito.Size = new System.Drawing.Size(68, 17);
            this.CBDeposito.TabIndex = 8;
            this.CBDeposito.Text = "Deposito";
            this.CBDeposito.UseVisualStyleBackColor = true;
            this.CBDeposito.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // CBRetiro
            // 
            this.CBRetiro.AutoSize = true;
            this.CBRetiro.Location = new System.Drawing.Point(384, 169);
            this.CBRetiro.Name = "CBRetiro";
            this.CBRetiro.Size = new System.Drawing.Size(54, 17);
            this.CBRetiro.TabIndex = 9;
            this.CBRetiro.Text = "Retiro";
            this.CBRetiro.UseVisualStyleBackColor = true;
            this.CBRetiro.CheckedChanged += new System.EventHandler(this.CBRetiro_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Cantidad";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // NUDCantidad
            // 
            this.NUDCantidad.Location = new System.Drawing.Point(129, 133);
            this.NUDCantidad.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.NUDCantidad.Name = "NUDCantidad";
            this.NUDCantidad.Size = new System.Drawing.Size(215, 20);
            this.NUDCantidad.TabIndex = 11;
            this.NUDCantidad.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.NUDCantidad);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CBRetiro);
            this.Controls.Add(this.CBDeposito);
            this.Controls.Add(this.BTCancelar);
            this.Controls.Add(this.BTAceptar);
            this.Controls.Add(this.NUDSaldo);
            this.Controls.Add(this.TBNumDeCuenta);
            this.Controls.Add(this.TBNombre);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.NUDSaldo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCantidad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TBNombre;
        private System.Windows.Forms.TextBox TBNumDeCuenta;
        private System.Windows.Forms.NumericUpDown NUDSaldo;
        private System.Windows.Forms.Button BTAceptar;
        private System.Windows.Forms.Button BTCancelar;
        private System.Windows.Forms.CheckBox CBDeposito;
        private System.Windows.Forms.CheckBox CBRetiro;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown NUDCantidad;
    }
}

