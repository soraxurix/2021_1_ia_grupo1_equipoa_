﻿namespace K_Means
{
    partial class dataDistancias
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dataDistancias));
            this.buttonLimpiar = new System.Windows.Forms.Button();
            this.buttonCalcular = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.dataPuntosFuentes = new System.Windows.Forms.DataGridView();
            this.dataCentroides = new System.Windows.Forms.DataGridView();
            this.dataDis = new System.Windows.Forms.DataGridView();
            this.dataIteraciones1 = new System.Windows.Forms.DataGridView();
            this.dataIteraciones2 = new System.Windows.Forms.DataGridView();
            this.PromedioC2X = new System.Windows.Forms.Label();
            this.textIteracionActual = new System.Windows.Forms.Label();
            this.dataInteracionesAnteriorC2 = new System.Windows.Forms.DataGridView();
            this.dataInteracionesAnteriorC1 = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelPromedioC1ActualX = new System.Windows.Forms.Label();
            this.labelPromedioC1ActualY = new System.Windows.Forms.Label();
            this.labelPromedioC2ActualY = new System.Windows.Forms.Label();
            this.labelPromedioC2ActualX = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelPromedioC1AnteriorY = new System.Windows.Forms.Label();
            this.labelPromedioC1AnteriorX = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.labelPromedioC2AnteriorY = new System.Windows.Forms.Label();
            this.labelPromedioC2AnteriorX = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.labelPromedioC3AnteriorY = new System.Windows.Forms.Label();
            this.labelPromedioC3AnteriorX = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labelPromedioC3ActualY = new System.Windows.Forms.Label();
            this.labelPromedioC3ActualX = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.dataInteracionesAnteriorC3 = new System.Windows.Forms.DataGridView();
            this.dataIteraciones3 = new System.Windows.Forms.DataGridView();
            this.labelPromedioC2ActualZ1 = new System.Windows.Forms.Label();
            this.labelPromedioC2ActualZ = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.labelPromedioC1ActualZ1 = new System.Windows.Forms.Label();
            this.labelPromedioC1ActualZ = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.labelPromedioC3ActualZ1 = new System.Windows.Forms.Label();
            this.labelPromedioC3ActualZ = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.labelPromedioC1AnteriorZ1 = new System.Windows.Forms.Label();
            this.labelPromedioC1AnteriorZ = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.labelPromedioC2AnteriorZ1 = new System.Windows.Forms.Label();
            this.labelPromedioC2AnteriorZ = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.labelPromedioC3AnteriorZ1 = new System.Windows.Forms.Label();
            this.labelPromedioC3AnteriorZ = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataPuntosFuentes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataCentroides)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataDis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataIteraciones1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataIteraciones2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataInteracionesAnteriorC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataInteracionesAnteriorC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataInteracionesAnteriorC3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataIteraciones3)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonLimpiar
            // 
            this.buttonLimpiar.BackColor = System.Drawing.Color.LightSkyBlue;
            this.buttonLimpiar.Enabled = false;
            this.buttonLimpiar.FlatAppearance.BorderSize = 0;
            this.buttonLimpiar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SteelBlue;
            this.buttonLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLimpiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonLimpiar.ForeColor = System.Drawing.Color.White;
            this.buttonLimpiar.Image = ((System.Drawing.Image)(resources.GetObject("buttonLimpiar.Image")));
            this.buttonLimpiar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLimpiar.Location = new System.Drawing.Point(145, 318);
            this.buttonLimpiar.Name = "buttonLimpiar";
            this.buttonLimpiar.Size = new System.Drawing.Size(123, 46);
            this.buttonLimpiar.TabIndex = 29;
            this.buttonLimpiar.Text = "Limpiar.";
            this.buttonLimpiar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonLimpiar.UseVisualStyleBackColor = false;
            // 
            // buttonCalcular
            // 
            this.buttonCalcular.BackColor = System.Drawing.Color.LightSkyBlue;
            this.buttonCalcular.FlatAppearance.BorderSize = 0;
            this.buttonCalcular.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SteelBlue;
            this.buttonCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCalcular.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonCalcular.ForeColor = System.Drawing.Color.White;
            this.buttonCalcular.Image = ((System.Drawing.Image)(resources.GetObject("buttonCalcular.Image")));
            this.buttonCalcular.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCalcular.Location = new System.Drawing.Point(11, 318);
            this.buttonCalcular.Name = "buttonCalcular";
            this.buttonCalcular.Size = new System.Drawing.Size(123, 46);
            this.buttonCalcular.TabIndex = 28;
            this.buttonCalcular.Text = "Calcular.";
            this.buttonCalcular.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonCalcular.UseVisualStyleBackColor = false;
            this.buttonCalcular.Click += new System.EventHandler(this.buttonCalcular_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.LightCoral;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Firebrick;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(274, 318);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(46, 46);
            this.button2.TabIndex = 23;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SteelBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(12, 251);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(228, 46);
            this.button1.TabIndex = 22;
            this.button1.Text = "Abrir documento de PF.";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label4.Location = new System.Drawing.Point(38, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(164, 18);
            this.label4.TabIndex = 41;
            this.label4.Text = "Puntos fuentes (PF).";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.LightSkyBlue;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SteelBlue;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(295, 251);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(282, 46);
            this.button3.TabIndex = 43;
            this.button3.Text = "Abrir documento de Centroides";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(380, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 18);
            this.label1.TabIndex = 44;
            this.label1.Text = "Centroides.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(679, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 18);
            this.label2.TabIndex = 46;
            this.label2.Text = "Distancias";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label3.Location = new System.Drawing.Point(901, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(146, 18);
            this.label3.TabIndex = 48;
            this.label3.Text = "Iteraciones actual.";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // dataPuntosFuentes
            // 
            this.dataPuntosFuentes.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataPuntosFuentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataPuntosFuentes.Location = new System.Drawing.Point(24, 39);
            this.dataPuntosFuentes.Name = "dataPuntosFuentes";
            this.dataPuntosFuentes.Size = new System.Drawing.Size(205, 206);
            this.dataPuntosFuentes.TabIndex = 49;
            // 
            // dataCentroides
            // 
            this.dataCentroides.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataCentroides.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataCentroides.Location = new System.Drawing.Point(328, 39);
            this.dataCentroides.Name = "dataCentroides";
            this.dataCentroides.Size = new System.Drawing.Size(205, 206);
            this.dataCentroides.TabIndex = 50;
            // 
            // dataDis
            // 
            this.dataDis.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataDis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataDis.Location = new System.Drawing.Point(619, 39);
            this.dataDis.Name = "dataDis";
            this.dataDis.Size = new System.Drawing.Size(205, 206);
            this.dataDis.TabIndex = 51;
            // 
            // dataIteraciones1
            // 
            this.dataIteraciones1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataIteraciones1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataIteraciones1.Location = new System.Drawing.Point(878, 70);
            this.dataIteraciones1.Name = "dataIteraciones1";
            this.dataIteraciones1.Size = new System.Drawing.Size(233, 109);
            this.dataIteraciones1.TabIndex = 52;
            // 
            // dataIteraciones2
            // 
            this.dataIteraciones2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataIteraciones2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataIteraciones2.Location = new System.Drawing.Point(878, 279);
            this.dataIteraciones2.Name = "dataIteraciones2";
            this.dataIteraciones2.Size = new System.Drawing.Size(233, 109);
            this.dataIteraciones2.TabIndex = 53;
            // 
            // PromedioC2X
            // 
            this.PromedioC2X.AutoSize = true;
            this.PromedioC2X.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PromedioC2X.ForeColor = System.Drawing.Color.DarkOrchid;
            this.PromedioC2X.Location = new System.Drawing.Point(616, 279);
            this.PromedioC2X.Name = "PromedioC2X";
            this.PromedioC2X.Size = new System.Drawing.Size(111, 18);
            this.PromedioC2X.TabIndex = 56;
            this.PromedioC2X.Text = "Iteración actual:";
            // 
            // textIteracionActual
            // 
            this.textIteracionActual.AutoSize = true;
            this.textIteracionActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textIteracionActual.ForeColor = System.Drawing.Color.DarkOrchid;
            this.textIteracionActual.Location = new System.Drawing.Point(733, 279);
            this.textIteracionActual.Name = "textIteracionActual";
            this.textIteracionActual.Size = new System.Drawing.Size(0, 18);
            this.textIteracionActual.TabIndex = 57;
            // 
            // dataInteracionesAnteriorC2
            // 
            this.dataInteracionesAnteriorC2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataInteracionesAnteriorC2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataInteracionesAnteriorC2.Location = new System.Drawing.Point(1172, 279);
            this.dataInteracionesAnteriorC2.Name = "dataInteracionesAnteriorC2";
            this.dataInteracionesAnteriorC2.Size = new System.Drawing.Size(233, 109);
            this.dataInteracionesAnteriorC2.TabIndex = 60;
            // 
            // dataInteracionesAnteriorC1
            // 
            this.dataInteracionesAnteriorC1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataInteracionesAnteriorC1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataInteracionesAnteriorC1.Location = new System.Drawing.Point(1172, 70);
            this.dataInteracionesAnteriorC1.Name = "dataInteracionesAnteriorC1";
            this.dataInteracionesAnteriorC1.Size = new System.Drawing.Size(233, 109);
            this.dataInteracionesAnteriorC1.TabIndex = 59;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label5.Location = new System.Drawing.Point(1195, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(159, 18);
            this.label5.TabIndex = 58;
            this.label5.Text = "Iteraciones anterior.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label6.Location = new System.Drawing.Point(875, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 18);
            this.label6.TabIndex = 62;
            this.label6.Text = "C1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label7.Location = new System.Drawing.Point(875, 258);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 18);
            this.label7.TabIndex = 63;
            this.label7.Text = "C2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label8.Location = new System.Drawing.Point(1169, 258);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 18);
            this.label8.TabIndex = 64;
            this.label8.Text = "C2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label9.Location = new System.Drawing.Point(1169, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 18);
            this.label9.TabIndex = 65;
            this.label9.Text = "C1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label10.Location = new System.Drawing.Point(875, 182);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 15);
            this.label10.TabIndex = 66;
            this.label10.Text = "Promedio X:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label11.Location = new System.Drawing.Point(875, 207);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 15);
            this.label11.TabIndex = 67;
            this.label11.Text = "Promedio Y:";
            // 
            // labelPromedioC1ActualX
            // 
            this.labelPromedioC1ActualX.AutoSize = true;
            this.labelPromedioC1ActualX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC1ActualX.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC1ActualX.Location = new System.Drawing.Point(956, 182);
            this.labelPromedioC1ActualX.Name = "labelPromedioC1ActualX";
            this.labelPromedioC1ActualX.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC1ActualX.TabIndex = 68;
            // 
            // labelPromedioC1ActualY
            // 
            this.labelPromedioC1ActualY.AutoSize = true;
            this.labelPromedioC1ActualY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC1ActualY.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC1ActualY.Location = new System.Drawing.Point(955, 207);
            this.labelPromedioC1ActualY.Name = "labelPromedioC1ActualY";
            this.labelPromedioC1ActualY.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC1ActualY.TabIndex = 69;
            // 
            // labelPromedioC2ActualY
            // 
            this.labelPromedioC2ActualY.AutoSize = true;
            this.labelPromedioC2ActualY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC2ActualY.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC2ActualY.Location = new System.Drawing.Point(955, 427);
            this.labelPromedioC2ActualY.Name = "labelPromedioC2ActualY";
            this.labelPromedioC2ActualY.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC2ActualY.TabIndex = 73;
            // 
            // labelPromedioC2ActualX
            // 
            this.labelPromedioC2ActualX.AutoSize = true;
            this.labelPromedioC2ActualX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC2ActualX.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC2ActualX.Location = new System.Drawing.Point(956, 402);
            this.labelPromedioC2ActualX.Name = "labelPromedioC2ActualX";
            this.labelPromedioC2ActualX.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC2ActualX.TabIndex = 72;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label14.Location = new System.Drawing.Point(875, 427);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 15);
            this.label14.TabIndex = 71;
            this.label14.Text = "Promedio Y:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label15.Location = new System.Drawing.Point(875, 402);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 15);
            this.label15.TabIndex = 70;
            this.label15.Text = "Promedio X:";
            // 
            // labelPromedioC1AnteriorY
            // 
            this.labelPromedioC1AnteriorY.AutoSize = true;
            this.labelPromedioC1AnteriorY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC1AnteriorY.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC1AnteriorY.Location = new System.Drawing.Point(1249, 207);
            this.labelPromedioC1AnteriorY.Name = "labelPromedioC1AnteriorY";
            this.labelPromedioC1AnteriorY.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC1AnteriorY.TabIndex = 77;
            // 
            // labelPromedioC1AnteriorX
            // 
            this.labelPromedioC1AnteriorX.AutoSize = true;
            this.labelPromedioC1AnteriorX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC1AnteriorX.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC1AnteriorX.Location = new System.Drawing.Point(1250, 182);
            this.labelPromedioC1AnteriorX.Name = "labelPromedioC1AnteriorX";
            this.labelPromedioC1AnteriorX.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC1AnteriorX.TabIndex = 76;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label18.Location = new System.Drawing.Point(1169, 207);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 15);
            this.label18.TabIndex = 75;
            this.label18.Text = "Promedio Y:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label19.Location = new System.Drawing.Point(1169, 182);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 15);
            this.label19.TabIndex = 74;
            this.label19.Text = "Promedio X:";
            // 
            // labelPromedioC2AnteriorY
            // 
            this.labelPromedioC2AnteriorY.AutoSize = true;
            this.labelPromedioC2AnteriorY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC2AnteriorY.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC2AnteriorY.Location = new System.Drawing.Point(1249, 427);
            this.labelPromedioC2AnteriorY.Name = "labelPromedioC2AnteriorY";
            this.labelPromedioC2AnteriorY.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC2AnteriorY.TabIndex = 81;
            // 
            // labelPromedioC2AnteriorX
            // 
            this.labelPromedioC2AnteriorX.AutoSize = true;
            this.labelPromedioC2AnteriorX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC2AnteriorX.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC2AnteriorX.Location = new System.Drawing.Point(1250, 402);
            this.labelPromedioC2AnteriorX.Name = "labelPromedioC2AnteriorX";
            this.labelPromedioC2AnteriorX.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC2AnteriorX.TabIndex = 80;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label22.Location = new System.Drawing.Point(1169, 427);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(74, 15);
            this.label22.TabIndex = 79;
            this.label22.Text = "Promedio Y:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label23.Location = new System.Drawing.Point(1169, 402);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 15);
            this.label23.TabIndex = 78;
            this.label23.Text = "Promedio X:";
            // 
            // labelPromedioC3AnteriorY
            // 
            this.labelPromedioC3AnteriorY.AutoSize = true;
            this.labelPromedioC3AnteriorY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC3AnteriorY.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC3AnteriorY.Location = new System.Drawing.Point(1249, 639);
            this.labelPromedioC3AnteriorY.Name = "labelPromedioC3AnteriorY";
            this.labelPromedioC3AnteriorY.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC3AnteriorY.TabIndex = 93;
            // 
            // labelPromedioC3AnteriorX
            // 
            this.labelPromedioC3AnteriorX.AutoSize = true;
            this.labelPromedioC3AnteriorX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC3AnteriorX.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC3AnteriorX.Location = new System.Drawing.Point(1250, 614);
            this.labelPromedioC3AnteriorX.Name = "labelPromedioC3AnteriorX";
            this.labelPromedioC3AnteriorX.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC3AnteriorX.TabIndex = 92;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label16.Location = new System.Drawing.Point(1169, 639);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 15);
            this.label16.TabIndex = 91;
            this.label16.Text = "Promedio Y:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label17.Location = new System.Drawing.Point(1169, 614);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 15);
            this.label17.TabIndex = 90;
            this.label17.Text = "Promedio X:";
            // 
            // labelPromedioC3ActualY
            // 
            this.labelPromedioC3ActualY.AutoSize = true;
            this.labelPromedioC3ActualY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC3ActualY.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC3ActualY.Location = new System.Drawing.Point(955, 639);
            this.labelPromedioC3ActualY.Name = "labelPromedioC3ActualY";
            this.labelPromedioC3ActualY.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC3ActualY.TabIndex = 89;
            // 
            // labelPromedioC3ActualX
            // 
            this.labelPromedioC3ActualX.AutoSize = true;
            this.labelPromedioC3ActualX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC3ActualX.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC3ActualX.Location = new System.Drawing.Point(956, 614);
            this.labelPromedioC3ActualX.Name = "labelPromedioC3ActualX";
            this.labelPromedioC3ActualX.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC3ActualX.TabIndex = 88;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label24.Location = new System.Drawing.Point(875, 639);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(74, 15);
            this.label24.TabIndex = 87;
            this.label24.Text = "Promedio Y:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label25.Location = new System.Drawing.Point(875, 614);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(75, 15);
            this.label25.TabIndex = 86;
            this.label25.Text = "Promedio X:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label26.Location = new System.Drawing.Point(1169, 470);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(27, 18);
            this.label26.TabIndex = 85;
            this.label26.Text = "C3";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label27.Location = new System.Drawing.Point(875, 470);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(27, 18);
            this.label27.TabIndex = 84;
            this.label27.Text = "C3";
            // 
            // dataInteracionesAnteriorC3
            // 
            this.dataInteracionesAnteriorC3.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataInteracionesAnteriorC3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataInteracionesAnteriorC3.Location = new System.Drawing.Point(1172, 491);
            this.dataInteracionesAnteriorC3.Name = "dataInteracionesAnteriorC3";
            this.dataInteracionesAnteriorC3.Size = new System.Drawing.Size(233, 109);
            this.dataInteracionesAnteriorC3.TabIndex = 83;
            // 
            // dataIteraciones3
            // 
            this.dataIteraciones3.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataIteraciones3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataIteraciones3.Location = new System.Drawing.Point(878, 491);
            this.dataIteraciones3.Name = "dataIteraciones3";
            this.dataIteraciones3.Size = new System.Drawing.Size(233, 109);
            this.dataIteraciones3.TabIndex = 82;
            // 
            // labelPromedioC2ActualZ1
            // 
            this.labelPromedioC2ActualZ1.AutoSize = true;
            this.labelPromedioC2ActualZ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC2ActualZ1.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC2ActualZ1.Location = new System.Drawing.Point(1081, 427);
            this.labelPromedioC2ActualZ1.Name = "labelPromedioC2ActualZ1";
            this.labelPromedioC2ActualZ1.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC2ActualZ1.TabIndex = 97;
            // 
            // labelPromedioC2ActualZ
            // 
            this.labelPromedioC2ActualZ.AutoSize = true;
            this.labelPromedioC2ActualZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC2ActualZ.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC2ActualZ.Location = new System.Drawing.Point(1082, 402);
            this.labelPromedioC2ActualZ.Name = "labelPromedioC2ActualZ";
            this.labelPromedioC2ActualZ.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC2ActualZ.TabIndex = 96;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label20.Location = new System.Drawing.Point(1001, 427);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(81, 15);
            this.label20.TabIndex = 95;
            this.label20.Text = "Promedio Z1:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label21.Location = new System.Drawing.Point(1001, 402);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 15);
            this.label21.TabIndex = 94;
            this.label21.Text = "Promedio Z:";
            // 
            // labelPromedioC1ActualZ1
            // 
            this.labelPromedioC1ActualZ1.AutoSize = true;
            this.labelPromedioC1ActualZ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC1ActualZ1.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC1ActualZ1.Location = new System.Drawing.Point(1081, 207);
            this.labelPromedioC1ActualZ1.Name = "labelPromedioC1ActualZ1";
            this.labelPromedioC1ActualZ1.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC1ActualZ1.TabIndex = 101;
            // 
            // labelPromedioC1ActualZ
            // 
            this.labelPromedioC1ActualZ.AutoSize = true;
            this.labelPromedioC1ActualZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC1ActualZ.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC1ActualZ.Location = new System.Drawing.Point(1082, 182);
            this.labelPromedioC1ActualZ.Name = "labelPromedioC1ActualZ";
            this.labelPromedioC1ActualZ.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC1ActualZ.TabIndex = 100;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label28.Location = new System.Drawing.Point(1001, 207);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(81, 15);
            this.label28.TabIndex = 99;
            this.label28.Text = "Promedio Z1:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label29.Location = new System.Drawing.Point(1001, 182);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(74, 15);
            this.label29.TabIndex = 98;
            this.label29.Text = "Promedio Z:";
            // 
            // labelPromedioC3ActualZ1
            // 
            this.labelPromedioC3ActualZ1.AutoSize = true;
            this.labelPromedioC3ActualZ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC3ActualZ1.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC3ActualZ1.Location = new System.Drawing.Point(1081, 639);
            this.labelPromedioC3ActualZ1.Name = "labelPromedioC3ActualZ1";
            this.labelPromedioC3ActualZ1.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC3ActualZ1.TabIndex = 105;
            // 
            // labelPromedioC3ActualZ
            // 
            this.labelPromedioC3ActualZ.AutoSize = true;
            this.labelPromedioC3ActualZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC3ActualZ.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC3ActualZ.Location = new System.Drawing.Point(1082, 614);
            this.labelPromedioC3ActualZ.Name = "labelPromedioC3ActualZ";
            this.labelPromedioC3ActualZ.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC3ActualZ.TabIndex = 104;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label32.Location = new System.Drawing.Point(1001, 639);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(81, 15);
            this.label32.TabIndex = 103;
            this.label32.Text = "Promedio Z1:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label33.Location = new System.Drawing.Point(1001, 614);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(74, 15);
            this.label33.TabIndex = 102;
            this.label33.Text = "Promedio Z:";
            // 
            // labelPromedioC1AnteriorZ1
            // 
            this.labelPromedioC1AnteriorZ1.AutoSize = true;
            this.labelPromedioC1AnteriorZ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC1AnteriorZ1.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC1AnteriorZ1.Location = new System.Drawing.Point(1381, 207);
            this.labelPromedioC1AnteriorZ1.Name = "labelPromedioC1AnteriorZ1";
            this.labelPromedioC1AnteriorZ1.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC1AnteriorZ1.TabIndex = 109;
            // 
            // labelPromedioC1AnteriorZ
            // 
            this.labelPromedioC1AnteriorZ.AutoSize = true;
            this.labelPromedioC1AnteriorZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC1AnteriorZ.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC1AnteriorZ.Location = new System.Drawing.Point(1382, 182);
            this.labelPromedioC1AnteriorZ.Name = "labelPromedioC1AnteriorZ";
            this.labelPromedioC1AnteriorZ.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC1AnteriorZ.TabIndex = 108;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label36.Location = new System.Drawing.Point(1301, 207);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(81, 15);
            this.label36.TabIndex = 107;
            this.label36.Text = "Promedio Z1:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label37.Location = new System.Drawing.Point(1301, 182);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(74, 15);
            this.label37.TabIndex = 106;
            this.label37.Text = "Promedio Z:";
            // 
            // labelPromedioC2AnteriorZ1
            // 
            this.labelPromedioC2AnteriorZ1.AutoSize = true;
            this.labelPromedioC2AnteriorZ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC2AnteriorZ1.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC2AnteriorZ1.Location = new System.Drawing.Point(1381, 427);
            this.labelPromedioC2AnteriorZ1.Name = "labelPromedioC2AnteriorZ1";
            this.labelPromedioC2AnteriorZ1.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC2AnteriorZ1.TabIndex = 113;
            // 
            // labelPromedioC2AnteriorZ
            // 
            this.labelPromedioC2AnteriorZ.AutoSize = true;
            this.labelPromedioC2AnteriorZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC2AnteriorZ.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC2AnteriorZ.Location = new System.Drawing.Point(1382, 402);
            this.labelPromedioC2AnteriorZ.Name = "labelPromedioC2AnteriorZ";
            this.labelPromedioC2AnteriorZ.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC2AnteriorZ.TabIndex = 112;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label40.Location = new System.Drawing.Point(1301, 427);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(81, 15);
            this.label40.TabIndex = 111;
            this.label40.Text = "Promedio Z1:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label41.Location = new System.Drawing.Point(1301, 402);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(74, 15);
            this.label41.TabIndex = 110;
            this.label41.Text = "Promedio Z:";
            // 
            // labelPromedioC3AnteriorZ1
            // 
            this.labelPromedioC3AnteriorZ1.AutoSize = true;
            this.labelPromedioC3AnteriorZ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC3AnteriorZ1.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC3AnteriorZ1.Location = new System.Drawing.Point(1381, 639);
            this.labelPromedioC3AnteriorZ1.Name = "labelPromedioC3AnteriorZ1";
            this.labelPromedioC3AnteriorZ1.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC3AnteriorZ1.TabIndex = 117;
            // 
            // labelPromedioC3AnteriorZ
            // 
            this.labelPromedioC3AnteriorZ.AutoSize = true;
            this.labelPromedioC3AnteriorZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioC3AnteriorZ.ForeColor = System.Drawing.Color.DarkOrchid;
            this.labelPromedioC3AnteriorZ.Location = new System.Drawing.Point(1382, 614);
            this.labelPromedioC3AnteriorZ.Name = "labelPromedioC3AnteriorZ";
            this.labelPromedioC3AnteriorZ.Size = new System.Drawing.Size(0, 15);
            this.labelPromedioC3AnteriorZ.TabIndex = 116;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label44.Location = new System.Drawing.Point(1301, 639);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(81, 15);
            this.label44.TabIndex = 115;
            this.label44.Text = "Promedio Z1:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.DarkOrchid;
            this.label45.Location = new System.Drawing.Point(1301, 614);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(74, 15);
            this.label45.TabIndex = 114;
            this.label45.Text = "Promedio Z:";
            // 
            // dataDistancias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1451, 690);
            this.Controls.Add(this.labelPromedioC3AnteriorZ1);
            this.Controls.Add(this.labelPromedioC3AnteriorZ);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.labelPromedioC2AnteriorZ1);
            this.Controls.Add(this.labelPromedioC2AnteriorZ);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.labelPromedioC1AnteriorZ1);
            this.Controls.Add(this.labelPromedioC1AnteriorZ);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.labelPromedioC3ActualZ1);
            this.Controls.Add(this.labelPromedioC3ActualZ);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.labelPromedioC1ActualZ1);
            this.Controls.Add(this.labelPromedioC1ActualZ);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.labelPromedioC2ActualZ1);
            this.Controls.Add(this.labelPromedioC2ActualZ);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.labelPromedioC3AnteriorY);
            this.Controls.Add(this.labelPromedioC3AnteriorX);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.labelPromedioC3ActualY);
            this.Controls.Add(this.labelPromedioC3ActualX);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.dataInteracionesAnteriorC3);
            this.Controls.Add(this.dataIteraciones3);
            this.Controls.Add(this.labelPromedioC2AnteriorY);
            this.Controls.Add(this.labelPromedioC2AnteriorX);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.labelPromedioC1AnteriorY);
            this.Controls.Add(this.labelPromedioC1AnteriorX);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.labelPromedioC2ActualY);
            this.Controls.Add(this.labelPromedioC2ActualX);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.labelPromedioC1ActualY);
            this.Controls.Add(this.labelPromedioC1ActualX);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dataInteracionesAnteriorC2);
            this.Controls.Add(this.dataInteracionesAnteriorC1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textIteracionActual);
            this.Controls.Add(this.PromedioC2X);
            this.Controls.Add(this.dataIteraciones2);
            this.Controls.Add(this.dataIteraciones1);
            this.Controls.Add(this.dataDis);
            this.Controls.Add(this.dataCentroides);
            this.Controls.Add(this.dataPuntosFuentes);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonLimpiar);
            this.Controls.Add(this.buttonCalcular);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "dataDistancias";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataPuntosFuentes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataCentroides)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataDis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataIteraciones1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataIteraciones2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataInteracionesAnteriorC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataInteracionesAnteriorC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataInteracionesAnteriorC3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataIteraciones3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonLimpiar;
        private System.Windows.Forms.Button buttonCalcular;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataGridView dataPuntosFuentes;
        private System.Windows.Forms.DataGridView dataCentroides;
        private System.Windows.Forms.DataGridView dataDis;
        private System.Windows.Forms.DataGridView dataIteraciones1;
        private System.Windows.Forms.DataGridView dataIteraciones2;
        private System.Windows.Forms.Label PromedioC2X;
        private System.Windows.Forms.Label textIteracionActual;
        private System.Windows.Forms.DataGridView dataInteracionesAnteriorC2;
        private System.Windows.Forms.DataGridView dataInteracionesAnteriorC1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelPromedioC1ActualX;
        private System.Windows.Forms.Label labelPromedioC1ActualY;
        private System.Windows.Forms.Label labelPromedioC2ActualY;
        private System.Windows.Forms.Label labelPromedioC2ActualX;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelPromedioC1AnteriorY;
        private System.Windows.Forms.Label labelPromedioC1AnteriorX;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label labelPromedioC2AnteriorY;
        private System.Windows.Forms.Label labelPromedioC2AnteriorX;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label labelPromedioC3AnteriorY;
        private System.Windows.Forms.Label labelPromedioC3AnteriorX;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelPromedioC3ActualY;
        private System.Windows.Forms.Label labelPromedioC3ActualX;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DataGridView dataInteracionesAnteriorC3;
        private System.Windows.Forms.DataGridView dataIteraciones3;
        private System.Windows.Forms.Label labelPromedioC2ActualZ1;
        private System.Windows.Forms.Label labelPromedioC2ActualZ;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label labelPromedioC1ActualZ1;
        private System.Windows.Forms.Label labelPromedioC1ActualZ;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label labelPromedioC3ActualZ1;
        private System.Windows.Forms.Label labelPromedioC3ActualZ;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label labelPromedioC1AnteriorZ1;
        private System.Windows.Forms.Label labelPromedioC1AnteriorZ;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label labelPromedioC2AnteriorZ1;
        private System.Windows.Forms.Label labelPromedioC2AnteriorZ;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label labelPromedioC3AnteriorZ1;
        private System.Windows.Forms.Label labelPromedioC3AnteriorZ;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
    }
}

