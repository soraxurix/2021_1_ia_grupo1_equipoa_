﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K_Means
{
    public class Matrizes
    {
        public string[,] PuntosInicales;
        public string[,] Centroides;
        public string[,] Distancias;
        public string[,] IteracionesActualC1;
        public string[,] IteracionesActualC2;
        public string[,] IteracionesAnteriorC1;
        public string[,] IteracionesAnteriorC2;
        public string[,] IteracionesAnteriorC3;
        public string[,] ListaC1;
        public string[,] ListaC2;
        public string[,] ListaC3;

        public string[,] getPuntosInicales()
        {
            return this.PuntosInicales;
        }
        public void setPuntosInicales(string[,] _PuntosInicales)
        {
            this.PuntosInicales = _PuntosInicales;
        }

        public string[,] getCentroides()
        {
            return this.Centroides;
        }
        public void setCentroides(string[,] _Centroides)
        {
            this.Centroides = _Centroides;
        }

        public string[,] getDistancias()
        {
            return this.Distancias;
        }
        public void setDistancias(string[,] _Distancias)
        {
            this.Distancias = _Distancias;
        }

        public string[,] getIteracionesActualC1()
        {
            return this.IteracionesActualC1;
        }

        public string[,] getIteracionesAnteriorC1()
        {
            return this.IteracionesAnteriorC1;
        }
        public void setIteracionesAnteriorC1(string[,] _Iteraciones)
        {
            this.IteracionesAnteriorC1 = _Iteraciones;
        }

        public string[,] getIteracionesAnteriorC2()
        {
            return this.IteracionesAnteriorC2;
        }
        public void setIteracionesAnteriorC2(string[,] _Iteraciones)
        {
            this.IteracionesAnteriorC3 = _Iteraciones;
        }

        public string[,] getIteracionesAnteriorC3()
        {
            return this.IteracionesAnteriorC3;
        }
        public void setIteracionesAnteriorC3(string[,] _Iteraciones)
        {
            this.IteracionesAnteriorC2 = _Iteraciones;
        }

        public string[,] getListaC1()
        {
            return this.ListaC1;
        }
        public void setListaC1(string[,] _ListaC1)
        {
            this.ListaC1 = _ListaC1;
        }

        public string[,] getListaC2()
        {
            return this.ListaC2;
        }
        public void setListaC2(string[,] _ListaC2)
        {
            this.ListaC2 = _ListaC2;
        }

        public string[,] getListaC3()
        {
            return this.ListaC3;
        }
        public void setListaC3(string[,] _ListaC3)
        {
            this.ListaC3 = _ListaC3;
        }
    }
}
