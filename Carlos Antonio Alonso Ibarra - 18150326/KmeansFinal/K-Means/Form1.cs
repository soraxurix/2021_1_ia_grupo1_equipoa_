﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace K_Means
{
    public partial class dataDistancias : Form
    {
        private Matrizes matrizes;
        //Se crean las variables para el promedio
        double promedioActualC1X = 0;
        double promedioActualC1Y = 0;
        double promedioActualC1Z = 0;
        double promedioActualC1Z1 = 0;
        //
        double promedioActualC2X = 0;
        double promedioActualC2Y = 0;
        double promedioActualC2Z = 0;
        double promedioActualC2Z1 = 0;
        //
        double promedioActualC3X = 0;
        double promedioActualC3Y = 0;
        double promedioActualC3Z = 0;
        double promedioActualC3Z1 = 0;

        /*Anteriores*/
        double promedioAnteriorC1X = 0;
        double promedioAnteriorC1Y = 0;
        double promedioAnteriorC1Z = 0;
        double promedioAnteriorC1Z1 = 0;
        //
        double promedioAnteriorC2X = 0;
        double promedioAnteriorC2Y = 0;
        double promedioAnteriorC2Z = 0;
        double promedioAnteriorC2Z1 = 0;
        //
        double promedioAnteriorC3X = 0;
        double promedioAnteriorC3Y = 0;
        double promedioAnteriorC3Z = 0;
        double promedioAnteriorC3Z1 = 0;

        Boolean EsIris = false;
        public dataDistancias()
        {
            InitializeComponent();
            matrizes = new Matrizes();
        }
        public string CalcularDistancia(int puntoX, int puntoY, double CentroideX, double CentroideY)
        {
            double distancia = 0;
            double hiponenusaCuadrada = 0;

            hiponenusaCuadrada = Math.Pow((puntoX - CentroideX), 2) + Math.Pow((puntoY - CentroideY), 2);

            distancia = Math.Pow(hiponenusaCuadrada, 0.5);

            return distancia.ToString();
        }
        public string CalcularDistanciaIris(double SepalX, double SepalY, double PetalX, double PetalY, double CentroideSepalX, double CentroideSepalY, double CentroidePetalX, double CentroidePetalY)
        {
            double distancia = 0;
            double hiponenusaCuadrada = 0;

            hiponenusaCuadrada = Math.Pow((SepalX - CentroideSepalX), 2) + Math.Pow((SepalY - CentroideSepalY), 2) + Math.Pow((PetalX - CentroidePetalX), 2) + Math.Pow((PetalY - CentroidePetalY), 2);

            distancia = Math.Pow(hiponenusaCuadrada, 0.5);

            return distancia.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //Se lee el archivo CSV y analiza para la creación de la matriz.
                string[] lineas = File.ReadAllLines(openFileDialog1.FileName);
                int contadorFilas = 0;
                int contadorColumnas = 0;
                foreach (var linea in lineas)//Se definen las dimensiones de la matriz.
                {
                    contadorFilas++;
                    var valores = linea.Split(',');
                    contadorColumnas = valores.Length;
                }

                string[,] PuntosInicales = new string[contadorFilas, contadorColumnas];

                for (int i = 0; i < lineas.Length; i++)//Se llena la matriz
                {
                    var linea = lineas[i];
                    var valores = linea.Split(',');
                    for (int j = 0; j < valores.Length; j++)
                    {
                        PuntosInicales[i, j] = valores[j];
                    }
                }
                if (PuntosInicales.GetLength(1) > 3)
                {
                    EsIris = true;
                }
                else
                {
                    EsIris = false;
                }
                matrizes.setPuntosInicales(PuntosInicales);

                DisplayData(dataPuntosFuentes,PuntosInicales);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //Se lee el archivo CSV y analiza para la creación de la matriz.
                string[] lineas = File.ReadAllLines(openFileDialog1.FileName);
                int contadorFilas = 0;
                int contadorColumnas = 0;
                foreach (var linea in lineas)//Se definen las dimensiones de la matriz.
                {
                    contadorFilas++;
                    var valores = linea.Split(',');
                    contadorColumnas = valores.Length;
                }

                string[,] Centroides = new string[contadorFilas, contadorColumnas];

                for (int i = 0; i < lineas.Length; i++)//Se llena la matriz
                {
                    var linea = lineas[i];
                    var valores = linea.Split(',');
                    for (int j = 0; j < valores.Length; j++)
                    {
                        Centroides[i, j] = valores[j];
                    }
                }
                matrizes.setCentroides(Centroides);

                DisplayData(dataCentroides, Centroides);
            }
        }
        int contador = 0;
        private void buttonCalcular_Click(object sender, EventArgs e)
        {
            if (EsIris) {
                do
                {
                    contador++;

                    IteracionesIris();
                } //while (contador==10); 
                while ((promedioAnteriorC1X != promedioActualC1X) || (promedioAnteriorC1Y != promedioActualC1Y) || (promedioAnteriorC1Z != promedioActualC1Z) || (promedioAnteriorC1Z1 != promedioActualC1Z1) ||
                         (promedioAnteriorC2X != promedioActualC2X) || (promedioAnteriorC2Y != promedioActualC2Y) || (promedioAnteriorC2Z != promedioActualC2Z) || (promedioAnteriorC2Z1 != promedioActualC2Z1) ||
                         (promedioAnteriorC3X != promedioActualC3X) || (promedioAnteriorC3Y != promedioActualC3Y) || (promedioAnteriorC3Z != promedioActualC3Z) || (promedioAnteriorC3Z1 != promedioActualC3Z1)
                );
            }
            else
            {                
                do
                {
                    contador++;

                    Iteraciones();
                    textIteracionActual.Text = contador.ToString();
                } while ((promedioAnteriorC1X != promedioActualC1X) || (promedioAnteriorC1Y != promedioActualC1Y) || (promedioAnteriorC2X != promedioActualC2X) || (promedioAnteriorC2Y != promedioActualC2Y));
            }            
        }

        public double Promedio(string[,] Iteracion, int Posición)
        {
            double promedio = 0;
            double sumatoria = 0;
            for(int i = 0; i< Iteracion.GetLength(0); i++)
            {
                sumatoria = sumatoria + Convert.ToDouble(Iteracion[i, Posición]);
            }
            promedio = sumatoria / Iteracion.GetLength(0);
            return promedio;
        }

        public void DisplayData(DataGridView tabla, string[,] matriz)//Toma la información de las matrices y las muestra en el datagrid
        {
            tabla.ColumnCount = matriz.GetLength(1);
            tabla.RowCount = matriz.GetLength(0);

            for (int i = 0; i < matriz.GetLength(0); i++)//Se llena el datagrid
            {
                for (int j = 0; j < matriz.GetLength(1); j++)
                {
                    tabla.Rows[i].Cells[j].Value = matriz[i, j];
                }
            }

            tabla.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            tabla.ColumnHeadersVisible = false;
            tabla.RowHeadersVisible = false;
        }
        public void Iteraciones()
        {            
            string[,] PuntosInicales = matrizes.getPuntosInicales();
            string[,] Centroides = matrizes.getCentroides();
            string[,] Distancias = new string[PuntosInicales.GetLength(0), PuntosInicales.GetLength(1) - 1];//Se le resta dos porqué sólo se ocupan dos valores
            DisplayData(dataCentroides, Centroides);

            promedioAnteriorC1X = promedioActualC1X;
            promedioAnteriorC1Y = promedioActualC1Y;

            promedioAnteriorC2X = promedioActualC2X;
            promedioAnteriorC2Y = promedioActualC2Y;

            for (int i = 0; i < Distancias.GetLength(0); i++)
            {
                for (int x = 0; x < Distancias.GetLength(1); x++)
                {
                    Distancias[i, x] = CalcularDistancia(Int32.Parse(PuntosInicales[i, 1]), Int32.Parse(PuntosInicales[i, 2]), Convert.ToDouble(Centroides[x, 1]), Convert.ToDouble(Centroides[x, 2]));
                }
            }

            DisplayData(dataDis, Distancias);



            List<int> ListaC1 = new List<int>();
            List<int> ListaC2 = new List<int>();
            
            for (int i = 0; i < Distancias.GetLength(0); i++)
            {
                if (Convert.ToDouble(Distancias[i, 0]) < Convert.ToDouble(Distancias[i, 1]))
                {
                    ListaC1.Add(i);
                }
                else if (Convert.ToDouble(Distancias[i, 0]) == Convert.ToDouble(Distancias[i, 1]))
                {
                    ListaC1.Add(i);
                }
                else
                {
                    ListaC2.Add(i);
                }
            }

            string[,] iteracionesC1 = new string[ListaC1.Count, PuntosInicales.GetLength(1)];
            string[,] iteracionesC2 = new string[ListaC2.Count, PuntosInicales.GetLength(1)];

            int[] arregloC1 = ListaC1.ToArray();
            int[] arregloC2 = ListaC2.ToArray();



            for (int i = 0; i < (iteracionesC1.GetLength(0)); i++)
            {
                for (int j = 0; j < iteracionesC1.GetLength(1); j++)
                {
                    iteracionesC1[i, j] = PuntosInicales[arregloC1[i], j];
                }
            }

            for (int i = 0; i < (iteracionesC2.GetLength(0)); i++)
            {
                for (int j = 0; j < iteracionesC2.GetLength(1); j++)
                {
                    iteracionesC2[i, j] = PuntosInicales[arregloC2[i], j];
                }
            }

            DisplayData(dataIteraciones1, iteracionesC1);
            DisplayData(dataIteraciones2, iteracionesC2);

            promedioActualC1X = Promedio(iteracionesC1, 1);
            promedioActualC1Y = Promedio(iteracionesC1, 2);

            promedioActualC2X = Promedio(iteracionesC2, 1);
            promedioActualC2Y = Promedio(iteracionesC2, 2);

            
            string[,] Promedios = new string[2, 2];
            for (int i = 0; i < 2; i++)//Se crea una matriz que guarde los promedios
            {
                for (int j = 0; j < 2; j++)
                {
                    switch (i)
                    {
                        case 0:
                            if (j == 0) Promedios[i, j] = promedioActualC1X.ToString();
                            if (j == 1) Promedios[i, j] = promedioActualC1Y.ToString();
                            break;

                        case 1:
                            if (j == 0) Promedios[i, j] = promedioActualC2X.ToString();
                            if (j == 1) Promedios[i, j] = promedioActualC2Y.ToString();
                            break;
                    }
                }
            }
            

            for (int i = 0; i < Centroides.GetLength(0); i++)//Se actualiza el Centroide
            {
                for (int j = 0; j < Centroides.GetLength(1)-1; j++)
                {
                    Centroides[i, j+1] = Promedios[i,j];
                }
            }

            if (contador != 1)
            {
                string[,] iteracionesC1Anterior = matrizes.getIteracionesAnteriorC1();
                string[,] iteracionesC2Anterior = matrizes.getIteracionesAnteriorC2();

                DisplayData(dataInteracionesAnteriorC1, iteracionesC1Anterior);
                DisplayData(dataInteracionesAnteriorC2, iteracionesC2Anterior);
            }

            matrizes.setIteracionesAnteriorC1(iteracionesC1);
            matrizes.setIteracionesAnteriorC2(iteracionesC2);
            matrizes.setCentroides(Centroides);

            

            labelPromedioC1ActualX.Text = promedioActualC1X.ToString();
            labelPromedioC1ActualY.Text = promedioActualC1Y.ToString();            
            labelPromedioC2ActualX.Text = promedioActualC2X.ToString();
            labelPromedioC2ActualY.Text = promedioActualC2Y.ToString();

            labelPromedioC1AnteriorX.Text = promedioAnteriorC1X.ToString();
            labelPromedioC1AnteriorY.Text = promedioAnteriorC1Y.ToString();
            labelPromedioC2AnteriorX.Text = promedioAnteriorC2X.ToString();
            labelPromedioC2AnteriorY.Text = promedioAnteriorC2Y.ToString();
        }

        public void IteracionesIris()
        {
            string[,] PuntosInicales = matrizes.getPuntosInicales();
            string[,] Centroides = matrizes.getCentroides();
            string[,] Distancias = new string[PuntosInicales.GetLength(0), PuntosInicales.GetLength(1) - 1];//Se le resta dos porqué sólo se ocupan dos valores
            DisplayData(dataCentroides, Centroides);
            
            promedioAnteriorC1X = promedioActualC1X;
            promedioAnteriorC1Y = promedioActualC1Y;
            promedioAnteriorC1Z = promedioActualC1Z;
            promedioAnteriorC1Z1 = promedioActualC1Z1;

            promedioAnteriorC2X = promedioActualC2X;
            promedioAnteriorC2Y = promedioActualC2Y;
            promedioAnteriorC2Z = promedioActualC2Z;
            promedioAnteriorC2Z1 = promedioActualC2Z1;

            promedioAnteriorC3X = promedioActualC3X;
            promedioAnteriorC3Y = promedioActualC3Y;
            promedioAnteriorC3Z = promedioActualC3Z;
            promedioAnteriorC3Z1 = promedioActualC3Z1;
            
            
            for (int i = 0; i < Distancias.GetLength(0); i++)
            {
                for (int x = 0; x < Distancias.GetLength(1); x++)
                {
                    Distancias[i, x] = CalcularDistanciaIris(Convert.ToDouble(PuntosInicales[i, 0]),
                        Convert.ToDouble(PuntosInicales[i, 1]),
                        Convert.ToDouble(PuntosInicales[i, 2]),
                        Convert.ToDouble(PuntosInicales[i, 3]),
                        Convert.ToDouble(Centroides[x, 1]),
                        Convert.ToDouble(Centroides[x, 2]),
                        Convert.ToDouble(Centroides[x, 3]),
                        Convert.ToDouble(Centroides[x, 4]));
                }
            }
            DisplayData(dataDis, Distancias);

            List<int> ListaC1 = new List<int>();
            List<int> ListaC2 = new List<int>();
            List<int> ListaC3 = new List<int>();
            //List<int> ListaC4 = new List<int>();

            for (int i = 0; i < Distancias.GetLength(0); i++)
            {
                if (Convert.ToDouble(Distancias[i, 0]) < Convert.ToDouble(Distancias[i, 1]) &&
                    Convert.ToDouble(Distancias[i, 0]) < Convert.ToDouble(Distancias[i, 2]))
                {
                    ListaC1.Add(i);
                }
                else if (Convert.ToDouble(Distancias[i, 1]) < Convert.ToDouble(Distancias[i, 0]) &&
                    Convert.ToDouble(Distancias[i, 1]) < Convert.ToDouble(Distancias[i, 2]))
                {
                    ListaC2.Add(i);
                }
                else if (Convert.ToDouble(Distancias[i, 2]) < Convert.ToDouble(Distancias[i, 0]) &&
                    Convert.ToDouble(Distancias[i, 2]) < Convert.ToDouble(Distancias[i, 1]))
                {
                    ListaC3.Add(i);
                }
            }

            if (ListaC1.Count != 0)//ListaC1
            {
                string[,] iteracionesC1Creacion = new string[ListaC1.Count, PuntosInicales.GetLength(1)];
                matrizes.setListaC1(iteracionesC1Creacion);
            }
            else
            {
                string[,] iteracionesC1Creacion = new string[1, PuntosInicales.GetLength(1)];
                matrizes.setListaC1(iteracionesC1Creacion);
            }

            if (ListaC2.Count != 0)//ListaC2
            {
                string[,] iteracionesC2Creacion = new string[ListaC2.Count, PuntosInicales.GetLength(1)];
                matrizes.setListaC2(iteracionesC2Creacion);
            }
            else
            {
                string[,] iteracionesC2Creacion = new string[1, PuntosInicales.GetLength(1)];
                matrizes.setListaC2(iteracionesC2Creacion);
            }

            if (ListaC3.Count != 0)//ListaC3
            {
                string[,] iteracionesC3Creacion = new string[ListaC3.Count, PuntosInicales.GetLength(1)];
                matrizes.setListaC3(iteracionesC3Creacion);
            }
            else
            {
                string[,] iteracionesC3Creacion = new string[1, PuntosInicales.GetLength(1)];
                matrizes.setListaC3(iteracionesC3Creacion);
            }

            string[,] iteracionesC1 = matrizes.getListaC1();
            string[,] iteracionesC2 = matrizes.getListaC2();
            string[,] iteracionesC3 = matrizes.getListaC3();

            int[] arregloC1 = ListaC1.ToArray();
            int[] arregloC2 = ListaC2.ToArray();
            int[] arregloC3 = ListaC3.ToArray();
            
            if (ListaC1.Count != 0)
            {
                for (int i = 0; i < (iteracionesC1.GetLength(0)); i++)
                {
                    for (int j = 0; j < iteracionesC1.GetLength(1); j++)
                    {
                        iteracionesC1[i, j] = PuntosInicales[arregloC1[i], j];
                    }
                }
            }
            if (ListaC2.Count != 0)
            {
                for (int i = 0; i < (iteracionesC2.GetLength(0)); i++)
                {
                    for (int j = 0; j < iteracionesC2.GetLength(1); j++)
                    {
                        iteracionesC2[i, j] = PuntosInicales[arregloC2[i], j];
                    }
                }
            }
            if (ListaC2.Count != 0)
            {
                for (int i = 0; i < (iteracionesC3.GetLength(0)); i++)
                {
                    for (int j = 0; j < iteracionesC3.GetLength(1); j++)
                    {
                        iteracionesC3[i, j] = PuntosInicales[arregloC3[i], j];
                    }
                }
            }                

            DisplayData(dataIteraciones1, iteracionesC1);
            DisplayData(dataIteraciones2, iteracionesC2);
            DisplayData(dataIteraciones3, iteracionesC3);

            promedioActualC1X = Promedio(iteracionesC1, 0);
            promedioActualC1Y = Promedio(iteracionesC1, 1);
            promedioActualC1Z = Promedio(iteracionesC1, 2);
            promedioActualC1Z1 = Promedio(iteracionesC1, 3);


            promedioActualC2X = Promedio(iteracionesC2, 0);
            promedioActualC2Y = Promedio(iteracionesC2, 1);
            promedioActualC2Z = Promedio(iteracionesC2, 2);
            promedioActualC2Z1 = Promedio(iteracionesC2, 3);

            promedioActualC3X = Promedio(iteracionesC3, 0);
            promedioActualC3Y = Promedio(iteracionesC3, 1);
            promedioActualC3Z = Promedio(iteracionesC3, 2);
            promedioActualC3Z1 = Promedio(iteracionesC3, 3);

            string[,] Promedios = new string[3, 4];
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    switch (i)
                    {
                        case 0:
                            if (j == 0) Promedios[i, j] = promedioActualC1X.ToString();
                            if (j == 1) Promedios[i, j] = promedioActualC1Y.ToString();
                            if (j == 2) Promedios[i, j] = promedioActualC1Z.ToString();
                            if (j == 3) Promedios[i, j] = promedioActualC1Z1.ToString();
                            break;

                        case 1:
                            if (j == 0) Promedios[i, j] = promedioActualC2X.ToString();
                            if (j == 1) Promedios[i, j] = promedioActualC2Y.ToString();
                            if (j == 2) Promedios[i, j] = promedioActualC2Z.ToString();
                            if (j == 3) Promedios[i, j] = promedioActualC2Z1.ToString();
                            break;

                        case 2:
                            if (j == 0) Promedios[i, j] = promedioActualC2X.ToString();
                            if (j == 1) Promedios[i, j] = promedioActualC3Y.ToString();
                            if (j == 2) Promedios[i, j] = promedioActualC3Z.ToString();
                            if (j == 3) Promedios[i, j] = promedioActualC3Z1.ToString();
                            break;
                    }
                }
            }


            for (int i = 0; i < Centroides.GetLength(0); i++)//Se actualiza el Centroide
            {
                for (int j = 0; j < Centroides.GetLength(1) - 1; j++)
                {
                    Centroides[i, j + 1] = Promedios[i, j];
                }
            }
            
            if (contador != 1)
            {
                string[,] iteracionesC1Anterior = matrizes.getIteracionesAnteriorC1();
                string[,] iteracionesC2Anterior = matrizes.getIteracionesAnteriorC2();
                string[,] iteracionesC3Anterior = matrizes.getIteracionesAnteriorC3();

                DisplayData(dataInteracionesAnteriorC1, iteracionesC1Anterior);
                DisplayData(dataInteracionesAnteriorC3, iteracionesC2Anterior);
                DisplayData(dataInteracionesAnteriorC2, iteracionesC3Anterior);
            }
            
            matrizes.setIteracionesAnteriorC1(iteracionesC1);
            matrizes.setIteracionesAnteriorC2(iteracionesC2);
            matrizes.setIteracionesAnteriorC3(iteracionesC3);
            matrizes.setCentroides(Centroides);

            labelPromedioC1ActualX.Text = promedioActualC1X.ToString();
            labelPromedioC1ActualY.Text = promedioActualC1Y.ToString();
            labelPromedioC1ActualZ.Text = promedioActualC1Z.ToString();
            labelPromedioC1ActualZ1.Text = promedioActualC1Z1.ToString();

            labelPromedioC2ActualX.Text = promedioActualC2X.ToString();
            labelPromedioC2ActualY.Text = promedioActualC2Y.ToString();
            labelPromedioC2ActualZ.Text = promedioActualC2Z.ToString();
            labelPromedioC2ActualZ1.Text = promedioActualC2Z1.ToString();

            labelPromedioC3ActualX.Text = promedioActualC3X.ToString();
            labelPromedioC3ActualY.Text = promedioActualC3Y.ToString();
            labelPromedioC3ActualZ.Text = promedioActualC3Z.ToString();
            labelPromedioC3ActualZ1.Text = promedioActualC3Z1.ToString();
            /*Promedios anteriores*/
            labelPromedioC1AnteriorX.Text = promedioAnteriorC1X.ToString();
            labelPromedioC1AnteriorY.Text = promedioAnteriorC1Y.ToString();
            labelPromedioC1AnteriorZ.Text = promedioAnteriorC1Z.ToString();
            labelPromedioC1AnteriorZ1.Text = promedioAnteriorC1Z1.ToString();

            labelPromedioC2AnteriorX.Text = promedioAnteriorC2X.ToString();
            labelPromedioC2AnteriorY.Text = promedioAnteriorC2Y.ToString();
            labelPromedioC2AnteriorZ.Text = promedioAnteriorC2Z.ToString();
            labelPromedioC2AnteriorZ1.Text = promedioAnteriorC2Z1.ToString();

            labelPromedioC3AnteriorX.Text = promedioAnteriorC3X.ToString();
            labelPromedioC3AnteriorY.Text = promedioAnteriorC3Y.ToString();
            labelPromedioC3AnteriorZ.Text = promedioAnteriorC3Z.ToString();
            labelPromedioC3AnteriorZ1.Text = promedioAnteriorC3Z1.ToString();
        }
    }
}